from django.contrib import admin

from .models import City, Street, Address


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    search_fields = ['name']


@admin.register(Street)
class StreetAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    list_filter = ['city']
    search_fields = ['name']


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ['id', 'city', 'street', 'house']
