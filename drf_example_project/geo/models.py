from django.db import models


class City(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.name


class Street(models.Model):
    name = models.CharField('Название', max_length=255)
    city = models.ForeignKey(City, verbose_name='Город', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Улица'
        verbose_name_plural = 'Улицы'

    def __str__(self):
        return self.name


class Address(models.Model):
    city = models.ForeignKey(City, verbose_name='Город', on_delete=models.PROTECT)
    street = models.ForeignKey(Street, verbose_name='Улица', on_delete=models.PROTECT)
    house = models.CharField('Дом', max_length=255)

    class Meta:
        verbose_name = 'Адрес'
        verbose_name_plural = 'Адреса'

    def __str__(self):
        return '{}, {}, {}'.format(self.city.name, self.street.name, self.house)
