from django.apps import AppConfig


class GeoConfig(AppConfig):
    app_name = 'geo'
    name = 'geo'
    verbose_name = 'Geo'
