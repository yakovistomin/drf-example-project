from drf_example_project.geo.api.exceptions import InvalidStreetNameError


def street_name_validator(value):
    if value == 'street1':
        raise InvalidStreetNameError
    return value
