from rest_framework import status
from rest_framework.exceptions import ValidationError


class InvalidCityNameError(ValidationError):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Такого города не существует."
    default_code = 'invalid_city_name'


class InvalidStreetNameError(ValidationError):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Плохое название улицы"
    default_code = 'invalid_street_name'
