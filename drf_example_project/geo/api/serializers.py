from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from drf_example_project.geo.api.exceptions import InvalidCityNameError, InvalidStreetNameError
from drf_example_project.geo.api.validators import street_name_validator
from drf_example_project.geo.models import City, Street


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'

    def validate_name(self, value):
        if value == 'city1':
            raise ValidationError('Плохое название города')
        elif value == 'city2':
            raise InvalidCityNameError
        elif value == 'city3':
            raise InvalidCityNameError('Переопределенное сообщение')
        elif value == 'city4':
            raise InvalidCityNameError(['Переопределенное сообщение1', 'Переопределенное сообщение2'])
        return value


class StreetSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255, validators=[street_name_validator])

    class Meta:
        model = Street
        fields = '__all__'

    def validate(self, data):
        name = data['name']
        city = data['city']
        if name == 'street2' and city.name == 'Москва':
            raise InvalidStreetNameError('В городе нет такой улицы.')
        if name == 'street3' and city.name == 'Москва':
            raise InvalidStreetNameError({"name": 'В городе нет такой улицы.'})
        return data
