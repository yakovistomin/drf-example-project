from rest_framework.viewsets import ModelViewSet

from drf_example_project.geo.api.serializers import CitySerializer, StreetSerializer
from drf_example_project.geo.models import City, Street


class CityViewSet(ModelViewSet):
    model = City
    queryset = City.objects.all()
    serializer_class = CitySerializer


class StreetViewSet(ModelViewSet):
    model = Street
    queryset = Street.objects.all()
    serializer_class = StreetSerializer
