from django.urls import include, path
from rest_framework.routers import DefaultRouter

from drf_example_project.geo.api.views import CityViewSet, StreetViewSet

app_name = 'geo'

router = DefaultRouter()
router.register(r'city', CityViewSet, basename='city')
router.register(r'street', StreetViewSet, basename='street')

urlpatterns = [
    path('', include(router.urls)),
]
