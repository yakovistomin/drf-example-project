from django.db import models


class ProductCategory(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField('Название', max_length=255)
    category = models.ForeignKey(ProductCategory, verbose_name='Категория', on_delete=models.CASCADE)
    price = models.DecimalField('Цена', decimal_places=2, max_digits=10)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name
