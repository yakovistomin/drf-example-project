from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import set_rollback


def exception_handler(exc, context):
    if isinstance(exc, Http404):
        exc = exceptions.NotFound()
    elif isinstance(exc, PermissionDenied):
        exc = exceptions.PermissionDenied()

    if isinstance(exc, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['Retry-After'] = '%d' % exc.wait

        errors_data = get_errors_data(exc)

        set_rollback()
        return Response({"data": {}, "errors": errors_data}, status=exc.status_code, headers=headers)

    return None


def get_errors_data(exc):
    if isinstance(exc.detail, dict):
        errors_data = exc.get_full_details()
    elif isinstance(exc.detail, list):
        errors_data = {
            api_settings.NON_FIELD_ERRORS_KEY: exc.get_full_details(),
        }
    else:
        errors_data = {
            api_settings.NON_FIELD_ERRORS_KEY: [
                exc.get_full_details(),
            ]
        }
    return errors_data
