from django.db import models

from drf_example_project.geo.models import Address


class Client(models.Model):
    name = models.CharField('Имя', max_length=255)
    birthday = models.DateField('Дата рождения', blank=True, null=True)
    address = models.ForeignKey(Address, blank=True, null=True, verbose_name='Адрес', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return self.name
