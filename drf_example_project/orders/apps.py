from django.apps import AppConfig


class OrderConfig(AppConfig):
    app_name = 'orders'
    name = 'orders'
    verbose_name = 'Order'
