from django.contrib import admin

from .models import Order, OrderItem


class OrderItemInline(admin.TabularInline):
    model = OrderItem


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'number', 'created', 'client']
    list_filter = ['client']
    search_fields = ['number']
    inlines = [OrderItemInline]
