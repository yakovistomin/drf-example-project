from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from drf_example_project.orders.api.serializers import (
    OrderSerializer, OrderItemSerializer, CustomActionRequestSerializer)
from drf_example_project.orders.models import Order, OrderItem


class OrderViewSet(ModelViewSet):
    model = Order
    queryset = Order.objects.all()

    @action(detail=False, methods=['POST'])
    def get_product_total_price(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(True)

        product = serializer.validated_data['product']
        count = serializer.validated_data['count']
        total = product.price * count

        return Response(data={'total': total, 'description': 'Какой-то текст'})

    @action(detail=False, methods=['GET'])
    def get_product_total_price2(self, request):
        serializer = self.get_serializer(data=request.GET)
        serializer.is_valid(True)

        product = serializer.validated_data['product']
        count = serializer.validated_data['count']
        total = product.price * count

        return Response(data={'total': total, 'description': 'Какой-то текст'})

    @action(detail=False, methods=['GET'])
    def get_product_total_price3(self, request):

        raise ValidationError('Просто ошибка')

        return Response(data={})

    @action(detail=False, methods=['GET'])
    def get_product_total_price4(self, request):
        raise ValidationError({'field_name': 'Просто ошибка'})

        return Response(data={})

    def get_serializer_class(self):
        if self.action.startswith('get_product_total_price'):
            return CustomActionRequestSerializer
        return OrderSerializer


class OrderItemViewSet(ModelViewSet):
    model = OrderItem
    serializer_class = OrderItemSerializer

    def dispatch(self, request, *args, **kwargs):
        order_id = kwargs.pop('order_id')
        self.order = get_object_or_404(Order, id=order_id)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = OrderItem.objects.all()
        if self.order:
            qs = qs.filter(order=self.order)
        return qs

    def perform_create(self, serializer):
        return serializer.save(order=self.order)
