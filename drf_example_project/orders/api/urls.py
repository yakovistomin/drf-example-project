from django.urls import include, path
from rest_framework.routers import DefaultRouter

from drf_example_project.orders.api.views import OrderViewSet, OrderItemViewSet

app_name = 'orders'

router = DefaultRouter()
router.register(r'order/(?P<order_id>[0-9]+)/items', OrderItemViewSet, basename='orderitem')
router.register(r'order', OrderViewSet, basename='order')
router.register(r'order/items', OrderItemViewSet, basename='orderitem')

urlpatterns = [
    path('', include(router.urls)),
]
