from django.core import validators as django_validators
from django.core.validators import MinValueValidator
from rest_framework import serializers

from drf_example_project.catalog.models import Product
from drf_example_project.orders.models import Order, OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ['id', 'product', 'count', 'price']


class OrderSerializer(serializers.ModelSerializer):
    number = serializers.CharField(max_length=255, validators=[django_validators.validate_slug])
    items = OrderItemSerializer(read_only=True, many=True)

    class Meta:
        model = Order
        fields = ['id', 'number', 'created', 'client', 'items']


class CustomActionRequestSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all(), label='Товар')
    count = serializers.IntegerField(validators=[MinValueValidator(10)])

    class Meta:
        fields = '__all__'
