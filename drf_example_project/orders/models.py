from django.db import models

from drf_example_project.catalog.models import Product
from drf_example_project.clients.models import Client


class Order(models.Model):
    number = models.CharField('Номер', max_length=100, unique=True)
    client = models.ForeignKey(Client, verbose_name='Клиент', on_delete=models.PROTECT)
    created = models.DateTimeField('Создан', auto_created=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return self.number


class OrderItem(models.Model):
    order = models.ForeignKey(Order, verbose_name='Заказ', related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.PROTECT)
    count = models.PositiveSmallIntegerField('Количество')
    price = models.DecimalField('Цена', decimal_places=2, max_digits=6)

    class Meta:
        verbose_name = 'Товар в заказе'
        verbose_name_plural = 'Товары в заказах'

    def __str__(self):
        return '{}, {}шт. x {}р.'.format(self.product, self.count, self.price)

    @property
    def amount(self):
        return self.price * self.count
